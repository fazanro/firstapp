import React from 'react';
import { StyleSheet, Button, View, Image } from 'react-native';
import { gyroscope, setUpdateIntervalForType, SensorTypes } from "react-native-sensors";

setUpdateIntervalForType(SensorTypes.gyroscope, 200);

let subscribe = null;

export default class Game extends React.Component {


    componentWillMount() {
        this.resetBeer()
    }

    componentDidMount() {
       subscribe =  gyroscope.subscribe(({ z }) => {
            this.setState({
                beerX: this.state.beerX - 5
            }, () => {
                if (z < 0) {
                    this.setState({ move: true, moveRight: true, moveLeft: false })
                } else {
                    if (z > 0) {
                        this.setState({ move: true, moveRight: false, moveLeft: true })
                    }
                }

                if (this.state.moveLeft && this.state.left > 5 || this.state.moveRight && this.state.left < 90)
                    if (this.state.move) this.setState({ left: this.state.left - 5 * (this.state.moveLeft ? 1 : -1) })

                if (this.state.beerX === 25  && !this.calcCollision()) {
                    this.resetBeer()
                } else {
                    if (this.state.beerX === 25 && this.calcCollision()) {
                        this.updateBeersCount()
                    }
                }
            })

        });
    }

    componentWillUnmount(){
        subscribe.unsubscribe()
    }

    static navigationOptions = ({ navigation }) => {
        const { state } = navigation;
        return {
            title: state.params ? `${state.params.title}` : '0 Beers',
        };
    };

    state = {
        beersCount: 0,
        left: 50,
        move: false,
        moveLeft: false,
        moveRight: false,
        beerX: null,
        beerY: null
    }

    updateBeersCount = () => {
        this.props.navigation.setParams({ title: `${this.state.beersCount + 1} Beers` })
        this.setState({ beersCount: this.state.beersCount + 1 })
        this.resetBeer()
    }

    calcCollision() {
        return Math.abs(this.state.beerY - this.state.left) <= 5
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    resetBeer() {
        let getY = this.getRandomInt(1, 18) * 5
        this.setState({
            beerX: 100,
            beerY: getY
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.gameArea}>
                    <View style={{ position: 'absolute', top: (90 - this.state.beerX) + '%', left: this.state.beerY + '%' }}><Image key={this.state.beerX} style={{ width: 25, height: 25 }} source={require('../resources/images/beer.png')} /></View>
                    <View style={{ left: this.state.left + '%', ...styles.player }}><Image style={{ width: 25 }} source={require('../resources/images/player.png')} /></View>
                    <View style={styles.button}>
                        <Button
                            title="Go to intro"
                            onPress={() => this.props.navigation.navigate('Home')}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    gameArea: {
        position: 'relative',
        width: '100%',
        height: '100%'
    },
    button: {
        position: 'absolute',
        bottom: 0,
        width: "100%"
    },
    player: {
        position: 'absolute',
        bottom: '10%'
    }
});