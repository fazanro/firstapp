import React from 'react';
import { StyleSheet, Image, View, Text, Button } from 'react-native';

export default class Intro extends React.Component {
    static navigationOptions = {
        title: 'Drinking with Giurgi'
    };
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Your name is Giurgi Bogdan and the task in this game si to catch as many beers as you can.</Text>
                <View style={styles.centerImage}><Image style={styles.player} source={require('../resources/images/player.png')} /></View>
                <View style={styles.button}>
                    <Button
                        title="Start Game"
                        onPress={() => this.props.navigation.navigate('Game')}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    text: {
        padding: 20
    },
    button: {
        position: 'absolute',
        bottom: 0,
        width: "100%"
    },
    player: {
        flex: 1,
        width: 10
    },
    centerImage: {
        flexDirection: 'row',
        alignItems: 'center'
    }
});