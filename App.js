import { createStackNavigator, createAppContainer } from 'react-navigation';
import GameScreen from './src/screens/Game';
import IntroScreen from './src/screens/Intro';

const MainNavigator = createStackNavigator({
    Home: { screen: IntroScreen },
    Game: { screen: GameScreen },
});

const App = createAppContainer(MainNavigator);

export default App;